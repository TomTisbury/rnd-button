﻿Public Class Form1

    Public Rn As New Random
    'Made By Tom https://tomtomnts.wordpress.com/
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        My.Settings.CurrentNumber = Rn.Next(1, 10)
        Select Case My.Settings.CurrentNumber
            Case 1
                Button1.Text = "I think I need some inspiration"
                Process.Start("http://randomcolour.com/")
                Threading.Thread.Sleep(1000)
                Button1.Text = "Press for something random!"

            Case 2
                Button1.Text = "Lets do some maths"
                Process.Start("calc")
                Threading.Thread.Sleep(1000)
                Button1.Text = "Press for something random!"

            Case 3
                Button1.Text = "This is not a paid endorsement"
                Process.Start("https://tomtomnts.wordpress.com/")
                Threading.Thread.Sleep(1000)
                Button1.Text = "Press for something random!"

            Case 4
                Button1.Text = "Lets draw"
                Process.Start("mspaint.exe")
                Threading.Thread.Sleep(1000)
                Button1.Text = "Press for something random!"

            Case 5
                Button1.Text = "I am thinking....."
                Process.Start("http://just-shower-thoughts.tumblr.com/")
                Threading.Thread.Sleep(1000)
                Button1.Text = "Press for something random!"

            Case 6
                Button1.Text = "You can never be too secure"
                Process.Start("https://www.random.org/passwords/?num=5&len=24&format=html&rnd=new")
                Threading.Thread.Sleep(1000)
                Button1.Text = "Press for something random!"

            Case 7
                Button1.Text = "Lets change the look up a bit"
                Me.BackgroundImage = My.Resources.Rainbow
                Threading.Thread.Sleep(1000)
                Button1.Text = "Press for something random!"

            Case 8
                Button1.Text = "lets hack the mainframe"
                Process.Start("cmd")
                Threading.Thread.Sleep(1000)
                Button1.Text = "Press for something random!"

            Case 9
                Button1.Text = "Let's listen to some music"
                Process.Start("https://www.randomlists.com/random-songs")
                Threading.Thread.Sleep(1000)
                Button1.Text = "Press for something random!"

            Case 10
                Button1.Text = "Suggest!"
                Process.Start("https://tomtomnts.wordpress.com/suggestions/")
                Threading.Thread.Sleep(1000)
                Button1.Text = "Press for something random!"
        End Select
    End Sub
    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Process.Start("https://tomtomnts.wordpress.com/")
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TopMost = True
        If My.Settings.FirstUse = False Then
            MsgBox("Welcome to the RNDButton program some of the websites used arne't mine and all the programs are microsofts. Hope You Enjoy")
            My.Settings.FirstUse = True
        End If
    End Sub
End Class
